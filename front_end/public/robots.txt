User-Agent: *
Disallow: /keywords/
Disallow: /install
Disallow: /search?
Disallow: /crates/faker

# seo scourge
User-agent: AhrefsBot
Disallow: /

User-agent: VelenPublicWebCrawler
Disallow: /

User-agent: SemrushBot
Disallow: /

User-agent: BLEXBot
Disallow: /

User-agent: MJ12bot
Disallow: /
